﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InindoWatch
{
    public partial class MainForm : Form
    {
        private Character[] characters;

        public MainForm()
        {
            InitializeComponent();
            Inindo.Connect();
        }

        private void RefreshCharacters()
        {
            characters = Inindo.GetCharacters();
            CharacterGrid.DataSource = characters;
            foreach (DataGridViewColumn column in CharacterGrid.Columns)
                CharacterGrid.Columns[column.Name].SortMode = DataGridViewColumnSortMode.Automatic;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            RefreshCharacters();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Inindo.Disconnect();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.RefreshCharacters();
        }

        private void CharacterGrid_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }
    }
}
