﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace InindoWatch
{
    public class Character
    {
        public const int SizeOf = 32;

        public Character(byte[] buffer, int offset)
        {
            this.Name = (CharacterName)buffer[offset + 0];
            this.U0 = buffer[offset + 1];
            this.MaxHealth = buffer[offset + 2] + (buffer[offset + 3] << 8);
            this.Health = buffer[offset + 4] + (buffer[offset + 5] << 8);
            this.MaxEnergy = buffer[offset + 6] + (buffer[offset + 7] << 8);
            this.Energy = buffer[offset + 8] + (buffer[offset + 9] << 8);
            this.ToNextLevel = buffer[offset + 10] + (buffer[offset + 11] << 8);
            this.Level = buffer[offset + 12];
            this.UB = buffer[offset + 13];
            this.Loyalty = buffer[offset + 14];
            this.Intelligence = buffer[offset + 15];
            this.Speed = buffer[offset + 16];
            this.Luck = buffer[offset + 17];
            this.Spells = (CharacterSpells)(buffer[offset + 18] + (buffer[offset + 19] << 8));
            this.Defense = buffer[offset + 20];
            this.Resistence = buffer[offset + 21];
            this.Power = buffer[offset + 22];
            this.Helmet = (CharacterItem)buffer[offset + 23];
            this.Body = (CharacterItem)buffer[offset + 24];
            this.Weapon = (CharacterItem)buffer[offset + 25];
            this.Projectile = (CharacterItem)buffer[offset + 26];
            this.Charm = (CharacterItem)buffer[offset + 27];
            this.Target = (CharacterName)buffer[offset + 28];
            this.Destination = (CharacterLocation)buffer[offset + 29];
            this.U9 = buffer[offset + 30];
            this.Flags = buffer[offset + 31];
        }

        public CharacterName Name { get; private set; }
        public CharacterGoal Goal
        {
            get { return (CharacterGoal)(this.Flags & 0x07); }
        }
        public CharacterLocation Destination { get; private set; }
        public CharacterName Target { get; private set; }

        public byte U0 { get; private set; }
        public byte UB { get; private set; }
        public byte U9 { get; private set; }

        public int MaxHealth { get; private set; }
        public int Health { get; private set; }
        public int MaxEnergy { get; private set; }
        public int Energy { get; private set; }
        public int ToNextLevel { get; private set; }
        public byte Level { get; private set; }
        public byte Loyalty { get; private set; }
        public byte Intelligence { get; private set; }
        public byte Speed { get; private set; }
        public byte Luck { get; private set; }
        public CharacterSpells Spells { get; private set; }
        public byte Defense { get; private set; }
        public byte Resistence { get; private set; }
        public byte Power { get; private set; }
        public CharacterItem Helmet { get; private set; }
        public CharacterItem Body { get; private set; }
        public CharacterItem Weapon { get; private set; }
        public CharacterItem Projectile { get; private set; }
        public CharacterItem Charm { get; private set; }

        public bool Hidden
        {
            get { return (this.Flags & 0x08) == 0x08; }
        }

        public CharacterCompatibility Compatibility
        {
            get { return (CharacterCompatibility)((this.Flags & 0x30) >> 4); }
        }

        public CharacterGender Gender
        {
            get { return (CharacterGender)((this.Flags & 0xC0) >> 6); }
        }

        public byte Flags { get; private set; }
    }

    public enum CharacterGender
    {
        Unknown = 0,
        Male = 1,
        Female
    }

    public enum CharacterCompatibility
    {
        Poor = 0,
        Fair,
        Good,
        Excellent
    }

    public enum CharacterGoal
    {
        HeadingTowards = 0,
        ChasingSomeone,
        FleeingFromSomeone,
        HeadingTowards2,
        DoingSomethingAt,
        TrainingAt,
        PreparingToLeave,
        LookingForYou
    }

    public enum CharacterLocation : byte
    {
        Mutsu = 0, Dewa, Rikuchu, Rikuzen, Hitachi, Echigo, Kozuke, Sagami,
        Boso, Kai, Totomi, Mikawa, Etchu, Mino, Echizen, Omi,
        Ise, Yamashiro, Tanba, Settsu, Kii, Harima, Izumo, Bitchu,
        Aki, Iyo, Tosa, Bungo, Hizen, Satsuma, Hiraizumi, Sado,
        Shinagawa, Nagasaki, Iga, Shirahama, Yamaguchi, Hyuga, Fuji, MtTsukuba,
        MtHaguro, MtOsore, Oshima, MtOntake, MtOchi, MtKen, MtAso, MtHiei,

        NONE = 255
    }

    public enum CharacterName : byte
    {
        Hero = 0, Kusou, SatoDenki, Harutomo, Kanemasa, Otowano, Naruse, Shiranui,
        Mochizuki, Hiryu, FumaKoji, Hanzo, Ichizo, Yukiyasha, Umeyasha, Kagero,
        Yakihime, Oniyuri, Tenkai, Kyonyo, Genso, Unryu, Keigan, Hannayo,
        Shurawaka, Suzaku, Bishamon, Yamanochi, Takeuchi, FuwaDanjo, ItoItosai, Mikogami,
        Saotome, Arashi, Toriyama, GouIkuma, Kuron, Tyohei, Kukou, Rakuo,
        Sou, Mikazuchi, Hyakureiko, Ashiya, Kamono, Tateoka, WadaSeiji, Tofumaru,
        Kagetoku, Gao, AmagiKoji, KenkoHosi, Indara, Jigyo, JoKosei, Yugasumi,
        Wakishima, Gennojo, Momochi, Rei, KashiKoji, Yagyu, Toshimasu, MaoKirizo,
        Kobungo, Yajiro, Ogawara, Hyodaru, Yoshimuri, Nekome, Sugitani, Jinnai,
        Senpu, Tobizaru, Unsui, DokanSojo, Jinen, Doan, Tosabo, Ragyu,
        Ryukai, IzumoSoji, Shoko, Kuramayama, Gono, Gashamon, Madara, Ingyo,
        Sekiwa, Takamitsu, Rizen, Dojin, Oufuku, Shinku, Shamaka, Kuzunoha,
        Hanyu, Akao, Jogasaki, Kashima, Kuroda, Shinbe, Kenpo, Hayami,
        Togen,

        NONE = 255
    }

    public enum CharacterItem : byte
    {
        Sword = 0, WoodSword, LongSword, GreatSword, Mace, Ninjato, Shuriken, Glaive,
        OakStaff, GoldStaff, WiseStaff, JoStick, Kusarigama, LongSpear, IronSword, YoshiBlade,
        BigGlaive, SteelBlade, FireStaff, Masamune, Helmet, WizardHat, SteelHelm, IronHelm,
        GreatHelm, IronHat, FaceMask, HardHat, Crown, DragonHat, Vest, Cloak,
        NinjaGarb, SageRobe, LightMail, ChainMail, HalfCoat, IronVest, Surplice, CourtDress,
        Cuirass, Hauberk, YoshiSuit, IronMesh, FullArmor, GoldSuit, SageMail, MageRobe,
        FancyRobe, MageClock, Star3, Star8, Shaken, ShortBow, LongBow, CrossBow,
        Pistol, Blowpipe, Musket, Arquebus, Amulet, Talisman, Scarab, Idol,
        WizardGem, Mandala, EnergySap, LionTail, Bracelet, KirinBone, KaraMono, Superblade,
        FireBlade, Muramasa, NinjaRod, Kusanagi, Multiblade, Scimitar, Stiletto, PowerRod,
        GoldRod, Coat, MetalHat, Sceptre, WarArmor, MagicArmor, IronMask, Headdress,
        DeerHelm, FurRobe, FaceGuard, Kabuto, GemArmor, GoldMail, HeavyCloak, Medicine,
        Elixir, Antidote, EnergyUp, BodyHealer, FireBomb, SmokeBomb, HealthFood, EnergyPill,
        TenguWing, Explosive, SleepBomb, ShinobiKey, MineKey, GateKey, DizzyGas, Password,
        GoldNugget, IronOre, Bomb, TimePiece, EelExtract, BubbleGum, TeaBowl, Porcelain,
        FireGem, EarthGem, SkyGem, MagicTorch, MagicRock, OdasDiary, CloudStone, PowerBook,
        HealthRock, NinjaCure, Resorer, Antifreeze, HealthKit, ZakuroRock, SpellBlock, SpiderWeb,
        TenguFan, DragonBook,

        NONE = 255,
    }

    [Flags]
    public enum CharacterSpells : ushort
    {
        Heal1 = 1,

    }

    public static class Inindo
    {
        private const string ProcessName = "snes9x1.51v5.2";
        private const int RAMOffset = 0x2AC7000;
        private const int RAMSize = 0x20000;
        private const int CharacterOffset = RAMOffset + 0xF0BC;
        private const int NumCharacters = 63;

        private static IntPtr processHandle;

        public static bool Connected { get; private set; }

        public static void Connect()
        {
            Process proc = Hax.GetProcess(ProcessName);
            processHandle = Hax.OpenProcess(Hax.ProcessAccessFlags.All, false, proc.Id);
            Connected = true;
        }

        public static void Disconnect()
        {
            Hax.CloseHandle(processHandle);
            Connected = false;
        }

        public static Character[] GetCharacters()
        {
            int charSize = Character.SizeOf;
            int bufferSize = NumCharacters * charSize;
            byte[] buffer = new byte[bufferSize];
            int bytesRead = 0;
            Hax.ReadProcessMemory(processHandle, new IntPtr(CharacterOffset), buffer, bufferSize, out bytesRead);
            if (bytesRead < bufferSize)
            {
                throw new Exception($"Only read {bytesRead} bytes, expected {bufferSize}");
            }
            
            List<Character> chars = new List<Character>();
            for (int i = 0; i < NumCharacters; i++)
            {
                Character ch = new Character(buffer, i * charSize);
                chars.Add(ch);
            }

            return chars.ToArray();
        }
    }
}
